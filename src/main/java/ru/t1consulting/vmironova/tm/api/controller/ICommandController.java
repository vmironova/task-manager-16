package ru.t1consulting.vmironova.tm.api.controller;

public interface ICommandController {

    void showVersion();

    void showInfo();

    void showAbout();

    void showCommands();

    void showArguments();

    void showHelp();

}
