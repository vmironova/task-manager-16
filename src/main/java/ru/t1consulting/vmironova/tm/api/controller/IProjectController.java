package ru.t1consulting.vmironova.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

}
