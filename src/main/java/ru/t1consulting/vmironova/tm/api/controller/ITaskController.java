package ru.t1consulting.vmironova.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void updateTaskById();

    void updateTaskByIndex();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

}
